#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <linux/auxvec.h>
#include <asm/cputable.h>

#define CPU_FLAG_ALTIVEC	1
#define CPU_FLAG_PS3		2

static unsigned char altivecAvailable() {
	unsigned char result = 0;
	unsigned long buf[64];
	ssize_t count;
	int i;
	int fd = open("/proc/self/auxv", O_RDONLY);
	if (fd < 0) return 0;
	do {
		count = read(fd, buf, sizeof(buf));
		if(count < 0) break;
		for(i = 0; i < (signed)(count / sizeof(unsigned long)); i += 2) {
			if(buf[i] == AT_HWCAP) {
				if(buf[i+1] & PPC_FEATURE_HAS_ALTIVEC) result |= CPU_FLAG_ALTIVEC;
				goto out_close;
			} else if (buf[i] == AT_NULL) {
				goto out_close;
			}
		}
	} while (count == sizeof(buf));
out_close:
	close(fd);
	return result;
}

static unsigned char ps3Available() {
	unsigned char result = 0;
	char buf[9];
	char expected_string[9] = {0x73,0x6f,0x6e,0x79,0x2c,0x70,0x73,0x33,0x00};
	FILE * fp = fopen("/proc/device-tree/compatible","rb");
	size_t control = fread(buf,9,1,fp);
	if(control == 1) {
		control = memcmp(buf,expected_string,9);
		if(!control) result |= CPU_FLAG_PS3;
	}
	fclose(fp);
	return result;
}

int main() {
	unsigned char flag_space = (altivecAvailable() | ps3Available());

	fputs("CPU_FLAGS_PPC=\"", stdout);
	if(flag_space & CPU_FLAG_ALTIVEC) fputs(" altivec", stdout);
	if(flag_space & CPU_FLAG_PS3) fputs(" ps3", stdout);
	fputs("\"\n", stdout);
	return 0;
}
